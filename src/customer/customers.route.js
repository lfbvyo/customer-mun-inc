const express = require('express');

const customerController = require('./customer.controller');
const router = express.Router();

/* GET all customers. */
router.get('/', customerController.getAllCustomers);

/* POST create customer. */
router.post('/new', customerController.createNewCustomer);

module.exports = router;
