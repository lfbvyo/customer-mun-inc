var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var customer = new Schema({
  name: String, // String is shorthand for {type: String}
  lastName: String,
  dateOfBirth: Date,
  email: { type: String, required: true },
  phoneNumber: String
});

module.exports = mongoose.model('Customer', customer);
