const Customer = require('./customer.model');

/**
 * Gets all customers on the data base
 */
const getAllCustomers = (req, res) => {
  Customer.find({}).exec((findError, customers) => {
    if (findError) {
      console.error(findError); // we could use a service to log errors here
      return res.status(400).json({ status: 'error', error: findError });
    }
    // we can format a response here or use a formatter function
    return res.status(200).json({ status: 'success', data: customers });
  });
};

/**
 * Creates a new customer
 */
const createNewCustomer = (req, res) => {
  const customerData = req.body;
  Customer.create(customerData, (createError, newCustomer) => {
    if (createError) {
      console.error(createError); // we could use a service to log errors here
      return res.status(400).json({ status: 'error', error: createError });
    }
    // we can format a response here or use a formatter function
    return res.status(201).json({ status: 'success', data: newCustomer });
  });
};

module.exports = {
  getAllCustomers,
  createNewCustomer
};
