// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

global.testDB = 'tests';
const app = require('./../app');

const expect = chai.expect;
// Configure chai
chai.use(chaiHttp);
chai.should();
let idToRemove;
describe('Customers_API', () => {
  describe('POST /api/customers/new', () => {
    // Creates a new customer
    it('Creates a new customer', done => {
      chai
        .request(app)
        .post('/api/customers/new')
        .type('application/json')
        .send({
          name: 'Felipe',
          lastName: 'Barnett',
          dateOfBirth: '1990/11/23',
          email: 'lfbv.yo@gmail.com',
          phoneNumber: '50660130065'
        })
        .end((err, res) => {
          expect(res).to.have.status(201);
          expect(res.body).should.be.a('object');
          expect(res.body).to.have.property('data');
          expect(res.body.data).to.be.an('object');
          expect(res.body.status).to.equal('success');
          expect(res.body.data.name).to.equal('Felipe');
          idToRemove = res.body.data._id;
          done();
        });
    });
    after(() => {
      mongoose.connection.db.dropDatabase();
    });
    it('Creates a new customer with bad date', done => {
      chai
        .request(app)
        .post('/api/customers/new')
        .type('application/json')
        .send({
          name: 'Felipe',
          lastName: 'Barnett',
          dateOfBirth: '1990/11/23g',
          email: 'lfbv.yo@gmail.com',
          phoneNumber: '50660130065'
        })
        .end((err, res) => {
          expect(res).to.have.status(400);
          expect(res.body).should.be.a('object');
          expect(res.body).to.have.property('error');
          expect(res.body.error).to.be.an('object');
          expect(res.body.status).to.equal('error');
          expect(res.body.error).to.have.property('errors');
          expect(res.body.error.errors.dateOfBirth).to.be.an('object');
          done();
        });
    });
  });

  describe('GET /api/customers/', () => {
    // Test to get all customer records
    it('should get all customer records', done => {
      chai
        .request(app)
        .get('/api/customers')
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).should.be.a('object');
          expect(res.body).to.have.property('data');
          expect(res.body.data).to.be.an('array');
          expect(res.body.status).to.equal('success');
          done();
        });
    });
  });
});
