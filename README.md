# Laboratorio Othree

Customer MUN Inc API has a database of customers with 2 REST services: get all customers and create new ones.

# Endpoints

- GET /api/customers | to get all customers
- POST /api/customers/new | to create a customer with the following data:

```json
{
  "name": "Felipe",
  "lastName": "Barnett",
  "dateOfBirth": "1990/11/23",
  "email": "lfbv.yo@gmail.com",
  "phoneNumber": "50660130065"
}
```

Note: dateOfBirth has to be a valid date.

## Installation

### Requirements

- Docker: https://docs.docker.com/install/

### Steps

- `git clone git@bitbucket.org:lfbvyo/customer-mun-inc.git`
- `cd customer-mun-inc`
- `docker-compose up`
- Go to localhost and check the available endpoints

### Tests

In order to run tests run: `docker exec -t express mocha`
