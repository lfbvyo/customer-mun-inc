const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');

const customersRouter = require('./src/customer/customers.route');

mongoose.connect(`mongodb://mongo/${global.testDB || 'num'}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/customers', customersRouter);

app.use((req, res) => {
  res.status(404).send({
    status: 'error',
    error: {
      message: 'Error 404 route not found'
    },
    availableEndpoints: ['GET /api/customers', 'POST /api/customers/new']
  });
});

module.exports = app;
