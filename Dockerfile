# Pull a node image from docker hub
FROM node:10 

# Set the working dir to /app
WORKDIR /app 

# Copy package.json to the container
COPY package.json package.json 

# Install package.json modules in container
RUN npm install 

# Copy everything to container /app
COPY . . 

# Expose port 3000 to mount it to another port in local machine 
EXPOSE 3000

# Install nodemon and mocha globally
RUN npm install -g nodemon mocha

# Start server inside container
CMD [ "nodemon", "bin/www" ]